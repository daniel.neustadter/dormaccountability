#!/usr/bin/env python3
from datetime import datetime

all_entering_people=   {"ssgt emteel":  ["mtl",          "0530", ],
                        "tsgt teal":    ["mtl",          "0530", ],
                        "amn snuffy":   ["student",      "0700", "phasei"],
                        "amn stu":      ["student",      "0730", "phaseii"],
                        #"steve shady":  ["unknown",      "0745", ],
                        "a1c dent":     ["student",      "0800", "phaseii"],
                        "tsgt dragon":  ["dragoneyes",   "0900", ],
                        "msgt ayes":    ["dragoneyes",   "0900", ],
                        "ssgt snufferson":["ncod",       "0930", ],
                        "ssgt cody":    ["ncod",         "0930", ],
                        #"johnny bravo": ["unknown",      "0945", ],
                        "testing":      ["student",      "6900", "phaseiv"]}

all_students=[]
all_mtl=[]
all_dragoneyes=[]
all_ncod=[]
all_unknown={   "johnny bravo": ["unknown",      "0945", ],
                "steve shady":  ["unknown",      "0745", ] }
people_entering_at_runtime=[]

status = 'status'

#Create a list of students
for key, val in all_entering_people.items():
    if val[0]=="student":
        all_students.append(val)

#Create a list of MTLs
for key, val in all_entering_people.items():
    if val[0] =="mtl":
        all_mtl.append(val)

#Create a list of DragonEyes
for key, val in all_entering_people.items():
    if val[0] == "dragoneyes":
        all_dragoneyes.append(val)

#Create a list of NCODs
for key, val in all_entering_people.items():
    if val[0] =="ncod":
        all_ncod.append(val)

#Create a list of unknowns
for key, val in all_entering_people.items():
    if val[0] =="unknown":
        all_unknown.append(val)

# #Variable Defined
# mtl = ('MTL')
# student = ('Student')
# dragon_eyes = ('DragonEyes')
# ncod = ('NCOD')
# other = ('other')
now = datetime.now()
current_time = now.strftime("%H:%M:%S")

incoming=bool(True)
yes_or_no = str(input('Is someone approaching? (type "yes" or "no" | default is yes): ').lower())
if yes_or_no == 'no':
    incoming = False

elif yes_or_no == 'yes':
    incoming = True

while incoming == True:
# print('How many people to be checked in for accountability?')
# how_many_to_loop = int(input(""))

# for x in range(how_many_to_loop):
    print('\nHalt! Who goes there?\n')
    print('What is their name?\n')

    approacher = input().lower()

    #Add approacher to to people entering at runtime
    #for key, val in all_entering_people.items():
    if val[0] == approacher:
        people_entering_at_runtime.append(val)
    else:
        people_entering_at_runtime.insert(0, approacher)
    print(people_entering_at_runtime)

    if approacher in all_unknown:
        print('Explicitly Prohibited from entry. DO NOT ALLOW ENTRY')

    #for i in range (all_entering_people(approacher:1))
    #status = 

    elif approacher in all_entering_people:
        print(approacher + ' the ')  #MY ENDING POINT
        print('success')
    else:
        print(approacher + ' was denied entry at ' + current_time)

    yes_or_no = str(input('\nIs someone else approaching? (type "yes" or "no" | default is no): ').lower())
    
    incoming=False
    if yes_or_no == 'no':
        incoming==False
        exit()
    elif yes_or_no == 'yes':
        incoming=True


# type_individual = input()

# if name_individual=
# for key, val in all_entering_people.items():
#     if val[1] =="Student":
#         for key1, val2, in all_students.items():
#             if val[1]==val



            

# #direct the type_individual input to the right variable
# if type_individual=='Student' or type_individual=='student':
#     type_individual=student
# elif type_individual=='MTL' or type_individual=='mtl':
#     type_individual = mtl
# elif type_individual=='DragonEyes' or type_individual=="dragoneyes":
#     type_individual==dragon_eyes
# elif type_individual=='NCOD' or type_individual=='ncod':
#     type_individual==ncod
# elif type_individual=='other':
#     print('Not Allowed in the building')
# else:
#     type_individual = other
#     print('Not Allowed in the building')

# type_individual_one = type_individual

# #Print the type of individual and the time
# print('\nThe type of individual is a/an ' + type_individual + '\n')
# now = datetime.now()

# #Define current_time
# current_time = now.strftime("%H:%M:%S")

# hour = int(now.strftime("%H"))

# student_1_is_late = False;
# if hour > 20:
#     student_1_is_late = True

# #if the type of individial is recognized, print the current time.
# if type_individual != other:
#     print(type_individual + ' was checked in at ' + current_time + '\n')
# else:
#     print(type_individual + ' was denied entry at '+ current_time + '\n')


# print('==================================')

# print('\nHalt! Who goes there?\n')

# print('Type in the type of individual:')
# print('Student, MTL, DragonEyes, NCOD, other\n')
# #select the type of individual
# type_individual = input()

# #direct the type_individual input to the right variable
# if type_individual=='Student' or type_individual=='student':
#     type_individual=student
# elif type_individual=='MTL' or type_individual=='mtl':
#     type_individual = mtl
# elif type_individual=='DragonEyes' or type_individual=="dragoneyes":
#     type_individual==dragon_eyes
# elif type_individual=='NCOD' or type_individual=='ncod':
#     type_individual==ncod
# elif type_individual=='other':
#     print('Not Allowed in the building')
# else: print('Not Allowed in the building')

# checked_in = True
# type_individual_two = type_individual

# #Print the type of individual and the time
# print('\nThe type of individual is a/an ' + type_individual + '\n')
# now = datetime.now()

# #Define current_time
# current_time = now.strftime("%H:%M:%S")

# hour = int(now.strftime("%H"))

# student_2_is_late = False;
# if hour > 20:
#     student_2_is_late = True

# if type_individual_two==type_individual_one:
#     checked_in=False

# #if the type of individial is recognized, print the current time.
# if type_individual == other:
#     print(type_individual + ' was denied entry at '+ current_time + '\n')
# else:
#     if checked_in==True:
#         print(type_individual + ' was checked in at ' + current_time + '\n')
#     if checked_in==False:
#         print(type_individual + ' was checked out at' + current_time+' \n')
 
# print('=============================')

# print('\nHalt! Who goes there?\n')

# print('Type in the type of individual:')
# print('Student, MTL, DragonEyes, NCOD, other\n')

# #select the type of individual
# type_individual = input()

# #direct the type_individual input to the right variable
# if type_individual=='Student' or type_individual=='student':
#     type_individual=student
# elif type_individual=='MTL' or type_individual=='mtl':
#     type_individual = mtl
# elif type_individual=='DragonEyes' or type_individual=="dragoneyes":
#     type_individual==dragon_eyes
# elif type_individual=='NCOD' or type_individual=='ncod':
#     type_individual==ncod
# elif type_individual=='other':
#     print('Not Allowed in the building')
# else: print('Not Allowed in the building')

# type_individual_three = type_individual

# #Print the type of individual and the time
# print('\nThe type of individual is a/an ' + type_individual + '\n')
# now = datetime.now()

# #Define current_time
# current_time = now.strftime("%H:%M:%S")
# hour = int(now.strftime("%H"))

# student_3_is_late = False;
# if hour > 20:
#     student_3_is_late = True

# if type_individual_three==type_individual_one or type_individual_three==type_individual_two:
#     checked_in=True
# if type_individual_three==type_individual_one and type_individual_three==type_individual_two:
#     checked_in=False
# if type_individual_three!=type_individual_one and type_individual_three!=type_individual_two:
#     checked_in=False
     

# #if the type of individial is recognized, print the current time.
# if type_individual == other:
#     print(type_individual + ' was denied entry at '+ current_time + '\n')
# else:
#     if checked_in==False:
#         print(type_individual + ' was checked in at ' + current_time + '\n')
#     if checked_in==True:
#         print(type_individual + ' was checked out at ' + current_time+' \n')


# print('\n==========================\n')


# if type_individual_one==student:
#     if student_1_is_late==True:
#         print('The first person to enter, a student, was late')
#     if student_1_is_late==False:
#         print('The first person to enter, a student, was on time')

# if type_individual_two==student:
#     if student_2_is_late==True:
#         print('The second person to enter, a student, was late')
#     if student_2_is_late==False:
#         print('The second person to enter, a student, was on time')

# if type_individual_three==student:
#     if student_3_is_late==True:
#         print('The third person to enter, a student, was late')
#     if student_2_is_late==False:
#         print('The third person to enter, a student, was on time')

# #=================================

# # Go to your local "DormAccountability" project.
# # If you have deleted your DormAccountability project, clone it again from Gitlab.

# # Given a list of members seeking entrance to the dormitory:
# # SSgt Emteel,MTL,0530
# # TSgt Teal,MTL,0530
# # Amn Snuffy,Student,0700,PhaseI
# # Amn Stu,Student,0730,PhaseII
# # Steve Shady,Unknown,0745
# # A1C Dent,Student,0800,PhaseII
# # TSgt Dragon,DragonEyes,0900
# # MSgt Ayes,DragonEyes,0900
# # SSgt Snufferson,NCOD,0930
# # SSgt Cody,NCOD,0930
# # Johnny Bravo,Unknown,0945

# # Create a list of Students, MTLs, DragonEyes, and NCOD members.
# # For each individual seeking entry into the dorm, make sure they are in one of these lists.
# # Output string should match one of the below formats:
# # {Name} the {Student/MTL/DragonEyes/NCOD} was {checked in/checked out} at {time}.
# # {Name} was denied entry at {time}.

# # Once all members have been processed, determine if any students arrived after 2300.

# # Students in Phase I are expected to be in at 2100.
# # For each of these students, find the time they last checked in and determine if any were late.
# # Output string should match the below format:
# # {Name} is a Phase I airman and was late to accountability(2100) at {time}.

# # For all students not in Phase I who arrived after 2300, print the below message:
# # Output string should match the below format:
# # {Name} was late to accountability(2300) at {time}.

# # Commit and push your changes to GitLab.