#!/usr/bin/env python3

from datetime import datetime

#Basic Time Calculations to get started
now = datetime.now()
current_time = now.strftime("%H%M")

#If someone is incoming, incoming=True, or nobody coming, incoming=false will end the program
incoming=bool(True)
#Checked in = True, Checked out = False
checked_in_or_out=bool()
in_or_out = 'default value'
kind = str()
kind_lower = kind.lower()

#Check whether or not the individual is a student, MTL, dragoneyes, etc
def get_kind(attempts, approacher):
    for key,value in attempts.items():
        if key == approacher:
            return(value[0])

#The original time checked in, pulled from the dictionary "attempts"
def get_time_first_checked_in(attempts, approacher):
    for key,value in attempts.items():
        if key == approacher:
            return(value[1])
#Access Locked list
def get_time_first_checked_in_locked(locked_attempts, approacher):
    for key,value in locked_attempts.items():
        if key == approacher:
            return(value[1])

#Retrieve status if checked in or not checked in from dictionary "attempts"
def get_if_checked_in(attempts, approacher):
    for key,value in attempts.items():
        if key == approacher:
            return(value[2])

#Get status if student is late
def get_phase(students, approacher):
    for key,value in students.items():
        if key == approacher:
            return(value)

# def set_new_time(attempts, approacher, current_time):
#     for key,value in attempts.items():
#         if key == approacher:
#             value[1] == current_time

# def set_checked_in_or_out(attempts, approacher):
#     for key,value in attempts.items():
#         if key == approacher:
#             value[2] != value[2]

# def set_new_time_on_new_entry(attempts, approacher, current_time):
#     for key,value in attempts.items():
#         if key == approacher:
#             value.update[2](current_time)

#Categories of members
students=   {   'Amn Snuffy': "PhaseI",
                'Amn Stu': "PhaseII",
                'A1C Dent': "PhaseII"}
ncods=      {   'SSgt Snufferson',
                'SSgt Cody'}
dragonEyes= {   'TSgt Dragon',
                'MSgt Ayes'}
mtls=       {   'SSgt Emteel',
                'TSgt Teal'}
unknown=    {   'Steve Shady', 
                'Johnny Bravo'}

#The original, given, list of attempted entries.
attempts = {
    'SSgt Emteel':        ['MTL',         '0530', True],
    'TSgt Teal':          ['MTL',         '0530', True],
    'Amn Snuffy':         ['Student',     '2200', True],
    'Amn Stu':            ['Student',     '2330', True],
    'Steve Shady':        ['Unknown',     '0745', True],
    'A1C Dent':           ['Student',     '0800', True],
    'TSgt Dragon':        ['DragonEyes',  '0900', True],
    'MSgt Ayes':          ['DragonEyes',  '0900', True],
    'SSgt Snufferson':    ['NCOD',        '0930', True],
    'SSgt Cody':          ['NCOD',        '0930', True],
    'Johnny Bravo':       ['Unknown',     '0945', True] }

locked_attempts = {
    'SSgt Emteel':        ['MTL',         '0530', True],
    'TSgt Teal':          ['MTL',         '0530', True],
    'Amn Snuffy':         ['Student',     '2200', True],
    'Amn Stu':            ['Student',     '2330', True],
    'Steve Shady':        ['Unknown',     '0745', True],
    'A1C Dent':           ['Student',     '0800', True],
    'TSgt Dragon':        ['DragonEyes',  '0900', True],
    'MSgt Ayes':          ['DragonEyes',  '0900', True],
    'SSgt Snufferson':    ['NCOD',        '0930', True],
    'SSgt Cody':          ['NCOD',        '0930', True],
    'Johnny Bravo':       ['Unknown',     '0945', True] }

runtime_entrants = []
phaseI_late=[]
phaseII_late=[]
phaseIII_late=[]
#print('XXXXXX' + str(get_if_checked_in(attempts, 'Amn Stu')))

#Determine whether or not to continue program. No input is required to continue as normal.
yes_or_no = str(input('\nAt the current time of ' + current_time + ', is someone approaching?\nPress ENTER to continue or type \'exit\' to quit: '))

#If 'exit' is typed, the program ends.
if yes_or_no == 'exit' or yes_or_no == 'q' or yes_or_no == 'stop':
    incoming = False

#The loop will continue until incoming is set to False.
while incoming == True:

    print('\nHalt! Who goes there?\n(What is their name? Case Sensitive)\n')
    approacher = input()
    #print(check_kind(attempts, approacher))
    print()
    kind=get_kind(attempts, approacher)
    time_first_checked_in=get_time_first_checked_in(attempts, approacher)
    time_first_checked_in_locked=get_time_first_checked_in_locked(locked_attempts, approacher)
       #time_first_checked_in == get_time_first_checked_in(attempts, approacher)
    if approacher in students:
        phase=get_phase(students, approacher)
        if phase == "PhaseI":
            time_first_checked_in = int(time_first_checked_in)
            if time_first_checked_in > 2100 or time_first_checked_in < 400:
                phaseI_late.append(approacher)
                
        elif phase == "PhaseII":
            time_first_checked_in = int(time_first_checked_in)
            if time_first_checked_in > 2200 or time_first_checked_in < 400:
                phaseII_late.append(approacher)
        elif phase == "PhaseIII":
            time_first_checked_in = int(time_first_checked_in)
            if time_first_checked_in > 2200 or time_first_checked_in < 400:
                phaseIII_late.append(approacher)


#When no input is given, the loop will restart itself.
    if approacher == '':
        print('You must enter a name! Restarting loop...')
        incoming == False
    # else:
    #     runtime_entrants.append(approacher)
    #     if approacher in attempts:
    #         runtime_entrants.append(get_kind(attempts, approacher))
    #     else:
    #         runtime_entrants.append("Unknown")
    #     runtime_entrants.append(current_time)
    #     print(runtime_entrants)

#When the input given is of someone not allowed, they are stopped from entry.
    elif approacher in unknown:
        print(approacher + ' was denied entry at ' + time_first_checked_in)
        incoming == False
    
#For all attempts
    elif approacher in attempts:
        if get_if_checked_in(attempts, approacher)==True:
            in_or_out ="checked in"
        elif get_if_checked_in(attempts, approacher)==False:
            in_or_out ="checked out"
        for key,value in attempts.items():
            if key == approacher:
                if value[2] == True:
                    value[2] = False
                elif value[2] == False:
                    value[2] = True
        for key,value in attempts.items():
            if key == approacher:
                value[1] = current_time
                # print(key+ ' is the key')
                # print(str(value[2]) + ' is the value')

        #FOR GRAMMAR LATER #if approacher in attempts and approacher in students:
        #print(get_if_checked_in(attempts, approacher))
        print(approacher + ' the ' + kind + ' was ' + in_or_out + ' at ' + str(time_first_checked_in))


    #time_first_checked_in == get_time_first_checked_in(attempts, approacher)
    # if approacher in students:
    #     phase=get_phase(students, approacher)
    #     if phase == "PhaseI":
    #         time_first_checked_in = int(time_first_checked_in)
    #         if time_first_checked_in > 2100 or time_first_checked_in < 400:
    #             phaseI_late.append(approacher)
                
    #     elif phase == "PhaseII":
    #         time_first_checked_in = int(time_first_checked_in)
    #         if time_first_checked_in > 2200 or time_first_checked_in < 400:
    #             phaseII_late.append(approacher)
    #     elif phase == "PhaseIII":
    #         time_first_checked_in = int(time_first_checked_in)
    #         if time_first_checked_in > 2200 or time_first_checked_in < 400:
    #             phaseIII_late.append(approacher)
                
    else:
        print(str(approacher) + ' was denied entry at ' + current_time)

#Repeat of the first block inside the loop. Determines whether to continue or exit.

    # #Print every late student
    # #for phaseI_late
    # for x in phaseI_late:
    #     print(str(x) + ' is a Phase I airman and was late to accountability(2100) at ' + str(time_first_checked_in))
    # #for phaseII_late
    # for x in phaseII_late:
    #     print(str(x) + ' is a Phase II airman and was late to accountability(2100) at ' + str(time_first_checked_in))
    # # #for phaseIII_late
    # for x in phaseIII_late:
    #     print(str(x) + ' is a Phase III airman and was late to accountability(2100 at ' + str(time_first_checked_in))

    # yes_or_no = str(input('\nIs someone else approaching? (Press ENTER to continue or type \'exit\' to quit): ').lower())
    
    yes_or_no = str(input('\nAt the current time of ' + current_time + ', is someone approaching?\nPress ENTER to continue or type \'exit\' to quit: '))
    #If 'exit' is typed, the program ends.
    if yes_or_no == 'exit' or yes_or_no == 'quit' or yes_or_no == 'stop' or yes_or_no == 'no' or yes_or_no == 'q':
        incoming=False
    elif yes_or_no == 'yes':
        incoming=True

print()

#Print every late student
#for phaseI_late
for x in phaseI_late:
    print(str(x) + ' is a Phase I airman and was late to accountability(2100) at ' + str(time_first_checked_in_locked))
#for phaseII_late
for x in phaseII_late:
    print(str(x) + ' is a Phase II airman and was late to accountability(2100) at ' + str(time_first_checked_in_locked))
# #for phaseIII_late
for x in phaseIII_late:
    print(str(x) + ' is a Phase III airman and was late to accountability(2100 at ' + str(locked_time_first_checked_in_locked))

#WHAT THE PROGRAM SHOULD DO:
#
#Is Someone approaching?
#Press ENTER to continue or type 'exit' to quit:
#
#Halt! Who goes there?
#
#What is their name?
#
# >student name
#
#{Student name} the {Student} was {checked in/out} at {time}
#
#{Name} is a Phase I airman and was late to accountability(2100) at {time}.
#
#Is someone else approaching? (Press ENTER to continue or type 'exit' to quit): >quit
#
#